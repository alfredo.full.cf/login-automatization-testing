    #language: es
    Característica: Login web InkaFarma
      Como un usuario
      Quiero loguearme en la web de InkaFarma
      Para validar sus funcionalidades

      @LoginInkafarma
      Esquema del escenario: Login - Iniciar sesión con credenciales correctas en la aplicación InkaFarma
        Dado un usuario carga la pagina web de InkaFarma
        Cuando clickea en el boton de iniciar sesión
        Y clickea en el boton ingresar con "<sTipo>"
        Y ingresa su correo "<sUser>"
        Y ingresa su contraseña "<sPassword>"
        Y clickea en boton siguiente
        Entonces Inicia sesion con exito y muestra el home de la aplicación
        Ejemplos:
          | sTipo    | sUser                       | sPassword    |
          | facebook | alfredo.full.cf@hotmail.com | L8kkfxY985   |
          | google   | alfredo.full.cf@gmail.com   | L8kkfxY984SX |

      @LoginInkafarma
      Esquema del escenario: Login - Iniciar sesión con usuario incorrecto en la aplicación InkaFarma
        Dado un usuario carga la pagina web de InkaFarma
        Cuando clickea en el boton de iniciar sesión
        Y clickea en el boton ingresar con "<sTipo>"
        Y ingresa su correo "<sUser>"
        Y clickea en boton siguiente
        Entonces La aplicacion muestra un mensaje de error "<sMensaje>"
        Ejemplos:
          | sTipo    | sUser                           | sMensaje                                                                  |
          | facebook | alfredo.full.cfa@hotmail.comaa  | El correo electrónico que has introducido no está conectado a una cuenta. |
          | google   | aalfredo.full.cfa@hotmail.comaa | No se ha podido encontrar tu cuenta de Google                             |

      @LoginInkafarma
      Esquema del escenario: Login - Iniciar sesión con usuario vacio en la aplicación InkaFarma
        Dado un usuario carga la pagina web de InkaFarma
        Cuando clickea en el boton de iniciar sesión
        Y clickea en el boton ingresar con "<sTipo>"
        Y ingresa su correo "<sUser>"
        Y clickea en boton siguiente
        Entonces La aplicacion muestra un mensaje de error "<sMensaje>"
        Ejemplos:
          | sTipo    | sUser | sMensaje                                                                                    |
          | facebook |       | El correo electrónico o número de móvil que has introducido no está conectado a una cuenta. |
          | google   |       | Introduce una dirección de correo electrónico o un número de teléfono                       |

      @LoginInkafarma
      Esquema del escenario: Login - Iniciar sesión con usuario correcto y contraseña incorrecta en la aplicación InkaFarma
        Dado un usuario carga la pagina web de InkaFarma
        Cuando clickea en el boton de iniciar sesión
        Y clickea en el boton ingresar con "<sTipo>"
        Y ingresa su correo "<sUser>"
        Y ingresa su contraseña "<sPassword>"
        Y clickea en boton siguiente
        Entonces La aplicacion muestra un mensaje de error "<sMensaje>"
        Ejemplos:
          | sTipo    | sUser                       | sPassword | sMensaje                                         |
          | facebook | alfredo.full.cf@hotmail.com | 123456789 | La contraseña que has introducido es incorrecta. |
          | google   | alfredo.full.cf@gmail.com   | 123456789 |                                                  |

      @LoginInkafarma2
      Esquema del escenario: Login - Iniciar sesión con usuario correcto y contraseña vacio en la aplicación InkaFarma
        Dado un usuario carga la pagina web de InkaFarma
        Cuando clickea en el boton de iniciar sesión
        Y clickea en el boton ingresar con "<sTipo>"
        Y ingresa su correo "<sUser>"
        Y ingresa su contraseña "<sPassword>"
        Y clickea en boton siguiente
        Entonces La aplicacion muestra un mensaje de error "<sMensaje>"
        Ejemplos:
          | sTipo    | sUser                       | sPassword | sMensaje                                         |
          | facebook | alfredo.full.cf@hotmail.com | 123456789 | La contraseña que has introducido es incorrecta. |
          | google   | alfredo.full.cf@gmail.com   | 123456789 |                                                  |
