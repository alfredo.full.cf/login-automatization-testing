package utilities;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class WebDriverDOM extends PageObject {


    protected void clickElement(WebElementFacade element) {
        element.click();
    }

    protected void mouseEvent(WebElementFacade element, WebElementFacade element2) throws Throwable {
        Actions builder = new Actions(getDriver());
        builder.moveToElement(element).perform();
        Thread.sleep(1000);
        clickElement(element2);
    }

    protected WebElement webDriverWait(WebElement element, int timeOnSeconds) {
        return new WebDriverWait(getDriver(), timeOnSeconds).until(ExpectedConditions.visibilityOf(element));
    }

    protected void waitForElementAndClick(WebElementFacade element, int timeOnSeconds) {
        webDriverWait(element, timeOnSeconds).click();
    }

    protected String getTextFromElement(WebElementFacade element) {
        return element.getText();
    }

    protected String waitForElementoAndGetText(WebElementFacade element, int timeOnSeconds) {
        return element.waitUntilVisible().getText();
    }

    protected void sendKeyElement(WebElementFacade webElement, String value) {
        webElement.sendKeys(value);
    }

    protected void waitForElementAndSendKey(WebElementFacade element, int timeOnSeconds, String value) {
        webDriverWait(element, timeOnSeconds).sendKeys(value);
    }

    protected boolean isElementPresent(WebElementFacade webElement) {
        try {
            webElement.isPresent();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    protected void waitForElement(WebElement element, int timeOnSeconds) {
        webDriverWait(element, timeOnSeconds);
    }

    protected boolean selectItem(List<WebElement> webElementList, String txt) {
        for (WebElement element : webElementList) {
            waitElementIsVisible(element, 10);
            System.out.println(element.getText());
            if (element.getText().contains(txt)) {
                element.click();
                return true;
            }
        }
        return false;
    }

    protected void clickElementInAList(List<WebElementFacade> listElement, String value) {
        waitUntilListHasElements(listElement, 10);
        for (WebElementFacade element : listElement) {
            System.out.println(" Encontrado: " + element.getText() + " - Buscado: " + value);
            if (element.getText().equalsIgnoreCase(value)) {
                element.click();
                break;
            }
        }
    }

    /**
     * Selecciona un elemento de la lista de un combo y le da click
     *
     * @param optionValue valor buscaro en el dropDown
     * @param element     elemento de tipo WebElementFacade
     */
    protected void selectOptionFromComboListAndClick(WebElement element, String optionValue) {
        Select select = new Select(element);
        for (WebElement webElement : select.getOptions()) {
            if (webElement.getText().contains(optionValue)) {
                webElement.click();
                break;
            }
        }
    }


    /**
     * Selecciona un elemento de la lista de un DropDown
     *
     * @param value           valor buscaro en el dropDown
     * @param element         elemento de tipo WebElementFacade
     * @param dropdownOptions lista de opciones del dropDown
     */
    protected void selectOnCustomDropdown(String value, WebElementFacade element, List<WebElementFacade> dropdownOptions) {
        waitUntilVisible(element, 20);
        clickElement(element);
        waitUntilListHasElements(dropdownOptions, 10);
        Assert.assertNotNull(dropdownOptions);
        Assert.assertFalse(dropdownOptions.isEmpty());
        for (WebElementFacade optionElement : dropdownOptions) {
            if (optionElement.getText().contains(value) || optionElement.getText().equalsIgnoreCase(value)) {
                clickElement(optionElement);
                return;
            }
        }
    }


    /**
     * Espera que el elemento sea visible y clickable durante un tiempo determinado
     *
     * @param element       lista de elementos de tipo WebElementFacade
     * @param timeOnSeconds tiempo de espera en segundos
     */
    protected void waitUntilReadyAndClick(WebElement element, int timeOnSeconds) {
        new WebDriverWait(getDriver(), timeOnSeconds).until(
                ExpectedConditions.and(
                        ExpectedConditions.visibilityOf(element),
                        ExpectedConditions.elementToBeClickable(element)
                )
        );
        element.click();
    }

    /**
     * Espera que el elemento sea visible durante un tiempo determinado
     *
     * @param element       lista de elementos de tipo WebElementFacade
     * @param timeOnSeconds tiempo de espera en segundos
     */
    @Deprecated
    protected void waitUntilVisible(WebElementFacade element, int timeOnSeconds) {
        element.waitUntilVisible();
        do {
            org.springframework.util.Assert.notNull(element, "El elemento a esperar no ha sido declarado. Es nulo.");
            if (element.isCurrentlyVisible() && element.isDisplayed() && element.isPresent()) {
                return;
            }
            timeOnSeconds--;
        } while (timeOnSeconds > 0);
    }

    /**
     * Espera que el elemento sea habilitado durante un tiempo determinado
     *
     * @param element       lista de elementos de tipo WebElementFacade
     * @param timeOnSeconds tiempo de espera en segundos
     */
    @Deprecated
    protected void waitUntilEnabled(WebElementFacade element, int timeOnSeconds) {
        element.waitUntilVisible();
        do {
            org.springframework.util.Assert.notNull(element, "El elemento a esperar no ha sido declarado. Es nulo.");
            if (element.isCurrentlyVisible() && element.isDisplayed() && element.isPresent() && element.isCurrentlyEnabled()) {
                return;
            }
            timeOnSeconds--;
        } while (timeOnSeconds > 0);
    }

    /**
     * Espera que el elemento se ecuentra en una lista de WebElementFacade en un tiempo establecido
     *
     * @param elementsList  lista de elementos de tipo WebElementFacade
     * @param timeOnSeconds tiempo de espera en segundos
     */
    @Deprecated
    protected void waitUntilListHasElements(List<WebElementFacade> elementsList, int timeOnSeconds) {
        do {
            if (elementsList != null && !elementsList.isEmpty()) {
                waitUntilVisible(elementsList.get(0), timeOnSeconds);
                return;
            }
            //implicitWait(1);
            timeOnSeconds--;
        } while (timeOnSeconds > 0);
        System.out.println("No se encontraron elementos en la lista.");
    }

    /**
     * Valida si el elemento es clickeable
     *
     * @param webElement elemento de tipo WebElementFacade
     */
    protected boolean isElementClickable(WebElementFacade webElement) {
        try {
            webElement.isClickable();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Realiza el scroll sobre el elemento indicado
     *
     * @param element elemento de tipo de WebElementFacade
     */
    protected void scrollToElement(WebElementFacade element) {
        Actions actions = new Actions(getDriver());
        actions.moveToElement(element);
        actions.perform();
    }

    /**
     * Te posiciona visualmente sobre el elemento indicado
     *
     * @param element elemento de tipo de WebElement
     */
    protected void scrollByJavaScriptToElement(WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("arguments[0].scrollIntoView();", element);
    }

    /**
     * Busca un valor entre mayúsculos y minúsculas
     * que coincida con el texto de los elementos enviados en la lista del WebElementFacade.
     *
     * @return Retorna -1 cuando no se encuentra el elemento.
     */
    protected Integer findElementInAList(List<WebElementFacade> listElement, String value) {
        waitUntilListHasElements(listElement, 10);
        Assert.assertNotNull(listElement);
        int i = 0;
        for (WebElementFacade element : listElement) {
            if (element.getText().equalsIgnoreCase(value)) {
                return i;
            }
            i++;
        }
        return -1;
    }

    /**
     * obtener el tipo del elemento
     *
     * @param webElement elemento de tipo de WebElement
     */
    protected void typeOnElement(WebElementFacade webElement, String value) {
        webElement.type(value);
    }


    /**
     * Entrega el valor de una etiqueta tipo "Input"
     *
     * @param element elemento de tipo de WebElement
     * @return texto, tipo string
     */
    protected String getAttributeValue(WebElement element) {
        return element.getAttribute("value");
    }


    /**
     * Verifica si el valor esta presente en la lista de WebElementFacade .
     *
     * @param listElement lista de tipo WebElement
     * @param value       valor ingresado para realizar la busqueda
     * @return verdadero, si el valor esta presente en la lista de WebElementFacade
     */
    protected Boolean IsPresentElementInList(List<WebElementFacade> listElement, String value) {
        waitUntilListHasElements(listElement, 10);
        for (WebElementFacade element : listElement) {
            System.out.println("Encontrado: " + element.getText() + " - Buscado: " + value);
            if (element.getText().equalsIgnoreCase(value)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Verifica si el valor se encuentra en la lista de WebElementFacade .
     *
     * @param listElement lista de tipo WebElement
     * @param value       valor ingresado para realizar la busqueda
     * @return verdadero, si el valor se encuentra en la lista de WebElementFacade
     */
    public static boolean verifyElementForAlist(List<WebElementFacade> listElement, String value) {
        Boolean existe = false;
        for (WebElementFacade element : listElement) {
            if (element.getText().equals(value)) {
                existe = true;
                break;
            }
        }
        return existe;
    }

    /**
     * Valida si el elemento es visible durante un tiempo establecido.
     *
     * @param element       elemento de tipo WebElement
     * @param timeOnSeconds tiempo en segundos (numeros enteros)
     * @return verdadero, elemento esperado esta visible
     */
    public boolean waitElementIsVisible(WebElement element, int timeOnSeconds) {
        boolean value = false;
        for (int i = 0; i <= timeOnSeconds; i++) {
            try {
                Thread.sleep(1000);
                value = element.isDisplayed();
                break;
            } catch (Exception e) {
                System.out.println(e.toString());
            }
        }
        return value;
    }

    /**
     * Valida si el elemento ya no es visible durante un tiempo establecido.
     *
     * @param element       elemento de tipo WebElement
     * @param timeOnSeconds tiempo en segundos (numeros enteros)
     * @return verdadero, elemento esperado no esta visible
     */
    public boolean waitElementIsNotVisible(WebElement element, int timeOnSeconds) {
        boolean value = false;
        for (int i = 0; i <= timeOnSeconds; i++) {
            try {
                Thread.sleep(1000);
                element.isDisplayed();
                if (i == timeOnSeconds) {
                    System.out.println("Tiempo de espera terminado, el elemento no llego a ocultarse");
                }
            } catch (Throwable e) {
                value = true;
                break;
            }
        }
        return value;
    }

    /**
     * Envío de una secuencia de texto a un elemento.
     *
     * @param element elemento de tipo WebElement
     */
    public void sendKeysAction(WebElementFacade element, String value) {
        getDriver().switchTo().frame(element);
        Actions actions = new Actions(getDriver());
        actions.sendKeys(element, value);
        actions.perform();
    }


}