package stepdefinition;

import cucumber.api.PendingException;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import cucumber.api.java.es.Y;
import net.thucydides.core.annotations.Steps;
import step.LoginInkaFarmaStep;

public class LoginInkaFarmaStepDefinition {
    @Steps
    LoginInkaFarmaStep loginInkaFarmaStep;


    @Dado("^un usuario carga la pagina web de InkaFarma$")
    public void unUsuarioCargaLaPaginaWebDeInkaFarma() throws Throwable {
        loginInkaFarmaStep.unUsuarioCargaLaPaginaWebDeInkaFarma();
    }

    @Cuando("^clickea en el boton de iniciar sesión$")
    public void clickeaEnElBotonDeIniciarSesión() throws Throwable {
        loginInkaFarmaStep.clickeaEnElBotonDeIniciarSesión();
    }

    @Entonces("^Inicia sesion con exito y muestra el home de la aplicación$")
    public void iniciaSesionConExitoYMuestraElHomeDeLaAplicación() throws Throwable {
        loginInkaFarmaStep.iniciaSesionConExitoYMuestraElHomeDeLaAplicación();
    }

    @Y("^clickea en el boton ingresar con \"([^\"]*)\"$")
    public void clickeaEnElBotonIngresarCon(String sTipo) throws Throwable {
        loginInkaFarmaStep.clickeaEnElBotonIngresarCon(sTipo);
    }

    @Y("^ingresa su correo \"([^\"]*)\"$")
    public void ingresaSuCorreo(String sUser) throws Throwable {
        loginInkaFarmaStep.ingresaSuCorreo(sUser);
    }

    @Y("^ingresa su contraseña \"([^\"]*)\"$")
    public void ingresaSuContraseña(String sPassword) throws Throwable {
        loginInkaFarmaStep.ingresaSuContraseña(sPassword);
    }

    @Y("^clickea en boton siguiente")
    public void clickeaEnBotonSiguiente() throws Throwable {
        loginInkaFarmaStep.clickeaEnBotonSiguiente();
    }

    @Entonces("^La aplicacion muestra un mensaje de error \"([^\"]*)\"$")
    public void laAplicacionMuestraUnMensajeDeError(String sMensaje) throws Throwable {
        loginInkaFarmaStep.laAplicacionMuestraUnMensajeDeError(sMensaje);
    }
}
