package page;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.support.FindBy;
import utilities.WebDriverDOM;

import java.util.Set;

@DefaultUrl("https://inkafarma.pe/")
public class LoginInkaFarmaPage extends WebDriverDOM {
    //Home page
    public static boolean _valuePage;
    public static String sTipoSesion;
    @FindBy(xpath = "//*[@class='slidedown-body-message']")
    private WebElementFacade txtPopUpPPreciosBajos;
    @FindBy(id = "braindw_register")
    private WebElementFacade txtPopUpRegister;
    @FindBy(id = "onesignal-slidedown-cancel-button")
    private WebElementFacade btnNoGracias;
    @FindBy(className = "braindw_closepop")
    private WebElementFacade btnCerrarPop;

    @FindBy(id = "lb--open-login-modal")
    private WebElementFacade btnIniciarSesion;
    @FindBy(id = "btn--login-google")
    private WebElementFacade btnIngresarConGoogle;
    @FindBy(id = "btn--login-facebook")
    private WebElementFacade btnIngresarConFacebook;
    @FindBy(id = "identifierId")
    private WebElementFacade txtEmailGoogle;
    @FindBy(xpath = "//*[@class='whsOnd zHQkBf']")
    private WebElementFacade txtPasswordGoogle;
    @FindBy(id = "email")
    private WebElementFacade txtEmailFacebook;
    @FindBy(id = "pass")
    private WebElementFacade txtPasswordFacebook;
    @FindBy(xpath = "//button[@class='VfPpkd-LgbsSe VfPpkd-LgbsSe-OWXEXe-k8QpJ VfPpkd-LgbsSe-OWXEXe-dgl2Hf nCP5yc AjY5Oe DuMIQc qIypjc TrZEUc lw1w4b']")
    private WebElementFacade btnSiguiente;
    @FindBy(id = "loginbutton")
    private WebElementFacade BtnIngresarConFacebook;
    @FindBy(xpath = "//*[@class='user-name']")
    private WebElementFacade txtNombreUsuario;

    public boolean validadPopUpRegister() throws Throwable {
        waitElementIsVisible(txtPopUpRegister, 5);
        if (isElementPresent(txtPopUpRegister)) {
            clickCerrarPop();
            return true;
        } else {
            return false;
        }
    }

    public boolean validarPopUpPreciosBajos() throws Throwable {
        waitElementIsVisible(txtPopUpPPreciosBajos, 5);
        if (isElementPresent(txtPopUpPPreciosBajos)) {
            clickNoGracias();
            return true;
        } else {
            return false;
        }
    }

    public void clickNoGracias() throws Throwable {
        waitElementIsVisible(btnNoGracias, 5);
        if (_valuePage == true) {
            if (isElementClickable(btnNoGracias)) {
                btnNoGracias.click();
            }
        }
    }

    public void clickCerrarPop() throws Throwable {
        waitElementIsVisible(btnCerrarPop, 5);
        if (_valuePage == true) {
            if (isElementClickable(btnCerrarPop)) {
                btnCerrarPop.click();
            }
        }
    }

    public boolean validarPagina(String paginaEsperada) throws Throwable {
        String paginaEncontrada = getDriver().getTitle();
        if (paginaEncontrada.equals(paginaEsperada)) {
            return _valuePage = true;
        } else {
            System.out.println("No se encontro la pagina : " + paginaEsperada);
            return _valuePage = false;
        }
    }

    public void clickIniciarSesion() throws Throwable {
        waitElementIsVisible(btnIniciarSesion, 5);
        if (_valuePage == true) {
            if (isElementClickable(btnIniciarSesion)) {
                btnIniciarSesion.click();
            }
        }
    }


    public boolean cambiarAVentanaSecundario() throws Throwable {
        String parentWindow = getDriver().getWindowHandle();
        Set<String> handles = getDriver().getWindowHandles();
        for (String windowHandle : handles) {
            if (!windowHandle.equals(parentWindow)) {
                getDriver().switchTo().window(windowHandle);
                return true;
            }
        }
        return false;

    }

    public void retornarVentantaPrincipal() throws Throwable {
        String parentWindow = getDriver().getWindowHandle();
        Set<String> handles = getDriver().getWindowHandles();
        for (String windowHandle : handles) {
            if (windowHandle.equals(parentWindow)) {
                getDriver().switchTo().window(windowHandle);
            }
        }
    }

    public void ingresarEmail(String sEmail) throws Throwable {
        switch (sTipoSesion) {
            case "google":
                waitElementIsVisible(txtEmailGoogle, 5);
                if (_valuePage == true) {
                    if (isElementPresent(txtEmailGoogle)) {
                        txtEmailGoogle.sendKeys(sEmail);
                        clickBotonSiguiente();
                    }
                }
                break;
            case "facebook":
                waitElementIsVisible(txtEmailFacebook, 5);
                if (_valuePage == true) {
                    if (isElementPresent(txtEmailFacebook)) {
                        txtEmailFacebook.sendKeys(sEmail);
                    }
                }
                break;
            default:
                break;
        }
    }

    public void ingresarPassword(String sPassword) throws Throwable {
        switch (sTipoSesion) {
            case "google":

                waitElementIsVisible(txtPasswordGoogle, 5);
                if (_valuePage == true) {
                    if (isElementPresent(txtPasswordGoogle)) {
                        txtPasswordGoogle.sendKeys(sPassword);
                    }
                }
                break;
            case "facebook":
                waitElementIsVisible(txtPasswordFacebook, 5);
                if (_valuePage == true) {
                    if (isElementPresent(txtPasswordFacebook)) {
                        txtPasswordFacebook.sendKeys(sPassword);
                    }
                }
                break;
            default:
                break;
        }
    }


    public boolean validarInicioDeSesion() throws Throwable {
        waitElementIsVisible(txtNombreUsuario, 5);
        if (isElementPresent(txtNombreUsuario)) {
            System.out.println("Se encontro usuario ");
            return true;
        } else {
            return false;
        }
    }

    public void clickBotonIngresarCon(String sTipo) throws Throwable {
        sTipoSesion = sTipo;
        switch (sTipoSesion) {
            case "google":
                waitElementIsVisible(btnIngresarConGoogle, 5);
                if (_valuePage == true) {
                    if (isElementClickable(btnIngresarConGoogle)) {
                        btnIngresarConGoogle.click();
                    }
                }
                cambiarAVentanaSecundario();
                break;
            case "facebook":
                waitElementIsVisible(btnIngresarConFacebook, 5);
                if (_valuePage == true) {
                    if (isElementClickable(btnIngresarConFacebook)) {
                        btnIngresarConFacebook.click();
                    }
                }
                cambiarAVentanaSecundario();
                break;
            default:
                break;
        }
    }

    public void clickBotonSiguiente() throws Throwable {
        switch (sTipoSesion) {
            case "google":
                waitElementIsVisible(btnSiguiente, 5);
                if (_valuePage == true) {
                    if (isElementClickable(btnSiguiente)) {
                        btnSiguiente.click();
                    }
                }
                break;
            case "facebook":
                waitElementIsVisible(BtnIngresarConFacebook, 5);
                if (_valuePage == true) {
                    if (isElementClickable(BtnIngresarConFacebook)) {
                        BtnIngresarConFacebook.click();
                    }
                }
                retornarVentantaPrincipal();
                break;
            default:
                break;
        }
    }

    ////////////////////
    @FindBy(xpath = "//*[@role='alert']")
    WebElementFacade txtMensajeErrorUsuarioIncorrectoFacebook;
    @FindBy(className = "o6cuMc")
    WebElementFacade txtMensajeErrorUsuarioIncorrectoGoogle;

    public boolean validarMensajeErrorUsuario(String sMensaje) throws Throwable {
        boolean valor = false;
        switch (sTipoSesion) {
            case "google":
                waitElementIsVisible(txtMensajeErrorUsuarioIncorrectoGoogle, 5);

                if (txtMensajeErrorUsuarioIncorrectoGoogle.getText().contains(sMensaje)) {
                    valor = true;
                } else {
                    valor = false;
                }
                break;
            case "facebook":
                waitElementIsVisible(txtMensajeErrorUsuarioIncorrectoFacebook, 5);

                if (txtMensajeErrorUsuarioIncorrectoFacebook.getText().contains(sMensaje)) {
                    valor = true;
                } else {
                    valor = false;
                }
                break;
            default:
                break;
        }

        return valor;
    }
}
